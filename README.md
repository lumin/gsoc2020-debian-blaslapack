# gsoc2020-debian-blaslapack

This git repository tracks the work during the [BLAS/LAPACK Ecosystem Enhancement (GSoC2020/Debian)](https://wiki.debian.org/SummerOfCode2020/Projects#SummerOfCode2020.2FApprovedProjects.2FBlasLapackEcosys.BLAS.2FLAPACK_Ecosystem_Enhancement
) project.

Proposal: https://people.debian.org/~lumin/debian-gsoc.pdf

## Documentation on Debian's BLAS/LAPACK

[Document](lalib.tex)

TODOs:

* [x] import document from another repo
* [ ] convert the documentation into md/rst
* [ ] refresh doc according to recent BLAS/LAPACK updates
* [ ] recommend high performance blas implementations

## Deprecating libcblas (status: ongoing)

https://lists.debian.org/debian-science/2020/05/msg00050.html

Status: already merged. lintian will report an INFO message when there is
ELF binary linked against `libcblas.so`.

TODOs:

* [x] warn the user if cblas is used

## Julia support (status: ongoing)

https://lists.debian.org/debian-science/2020/05/msg00032.html

TODOs:

* [x] update package and pass NEW queue
* [ ] debug the weird segfault / error when building julia

## libflame -- a better LAPACK (status: ongoing)

Already present in unstable: https://tracker.debian.org/pkg/libflame

TODOs:

* [ ] patching work (align with lapack ABI)
* [ ] patching work (lapack shared objects)
* [ ] lapack alternative

## Routine/Misc Tasks

1. Uploaded Openblas 3.10+ds
2. Added new autopkgtest case for scipy with BLAS=blis

## Threading-aware virtual blas (status: proposed->halt)

RFC https://lists.debian.org/debian-science/2020/05/msg00023.html

## Bumping system default blas/lapack provider (status: proposed->halt)

https://lists.debian.org/debian-science/2020/06/msg00022.html

Conclusion: different BLAS/LAPACK have slightly different behaviour.
Keeping the netlib blas/lapakc as the default provider is a preferrable choice,
lest some reverse dependencies fail to build due to test errors.

